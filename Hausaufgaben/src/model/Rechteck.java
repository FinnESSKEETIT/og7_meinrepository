package model;

import java.awt.Color;

public class Rechteck {

private int x;
private int y;
private int breite;
private int hoehe;

public Rechteck() {
    this.x = 0;
    this.y = 0;
    this.breite = 0;
    this.hoehe = 0;
}

public Rechteck(int x, int y, int breite, int hoehe) {
    this.x = x;
    this.y = y;
    this.breite = breite;
    this.hoehe = hoehe;
}

public int getY() {
    return y;
}

public int getX() {
    return x;
}

public int getBreite() {
    return breite;
}

public int getHoehe() {
    return hoehe;
}

public void setX(int x) {
    this.x = x;
}

public void setY(int y) {
    this.y = y;
}

public void setBreite(int breite) {
    this.breite = breite;
}

public void setHoehe(int hoehe) {
    this.hoehe = hoehe;
}

@Override
public String toString () {
    return "Rechteck [x=" +this.getX()  +", y=" +this.getY() + ", breite=" +this.getBreite() +", hoehe=" +this.getHoehe() +"]";
	}

public static void setcolor(Color black) {
	// TODO Auto-generated method stub
	
}
}
