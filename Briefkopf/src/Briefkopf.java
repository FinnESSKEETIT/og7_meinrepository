
import java.awt.*;
import java.awt.print.*;
public class Briefkopf{
  private Person absender ;
  private Person empfaenger;
  String betreff;
  String datum;
public void drucken(){
      Person empfaenger = new Person ("Herr", "Finn", "Schulte","","62","Deutschland","Berlin","12305","Haarlemer Weg");
    Person absender = new Person ("Herr", "Justin", "Ochmann","","82","Deutschland","Berlin","12524","Halker Zeile");  
  //Druckauftag beantragen
  PrinterJob pjob = PrinterJob.getPrinterJob();
  if (pjob.printDialog() == false)
    return;
  
  //Druckauftrag f�llen - eine Klasse Printable wird ben�tigt
  pjob.setPrintable(new Printable() {   
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
      //Anzahl der Seiten
      if (pageIndex >= 1)
        return Printable.NO_SUCH_PAGE;
      
      //Layout (Zeilenabstand)
      int zeile = 5, fontsize = 12;
      //Schriftart setzen
      g.setFont(new Font("Arial", Font.PLAIN, fontsize));
      
      //auf die Druckerseite zeichnen
      g.drawString(absender.getAnrede(), 60, zeile++ * (fontsize+2));
      g.drawString(absender.getVorname() + " " + absender.getNachname(), 60, zeile++ * (fontsize+2));
      g.drawString(absender.getAdresszusatz(), 60, zeile++ * (fontsize+2));
      g.drawString(absender.getStrasse() + " " + absender.getHausnummer(), 60, zeile++ * (fontsize+2));
      g.drawString(absender.getLand()+absender.getPlz()+" "+absender.getOrt(),60,zeile++*(fontsize+2));
  
      zeile +=3;
      g.setFont(new Font("Arial", Font.BOLD, fontsize));

      g.drawString(empfaenger.getAnrede(), 60, zeile++ * (fontsize+2));
      g.drawString(empfaenger.getVorname()+" "+empfaenger.getNachname(), 60, zeile++ * (fontsize+2));
      g.drawString(empfaenger.getAdresszusatz(), 60, zeile++ * (fontsize+2));
      g.drawString(empfaenger.getStrasse()+" "+empfaenger.getHausnummer(), 60, zeile++ * (fontsize+2));
g.drawString(empfaenger.getLand()+empfaenger.getPlz()+" "+empfaenger.getOrt(),60,zeile++*(fontsize+2));
      
      zeile +=2;
   //   g.drawString(betreff + " vom " + datum, 60, zeile * (fontsize+2));
      
      //Seite best�tigen
      return Printable.PAGE_EXISTS;
    }
  });
       
    try {
      pjob.print();
    } catch(Exception e) {
      System.out.println("Auftrag konnte nicht geladt werden ");
      e.printStackTrace();
    } finally {
      
    } // end of try
  
  } }
  
  
