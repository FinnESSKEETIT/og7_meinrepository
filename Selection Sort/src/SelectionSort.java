
public class SelectionSort {
  
	public static int[]selectionsort(int[]sort){
		for(int i=0;i<sort.length-1;i++) {
			for(int j=i+1;j<sort.length-1;j++) {
				if(sort[j]<sort[i]) {
					int speicher=sort[i];
					sort[i]=sort[j];
					sort[j]=speicher;
				}
			}
		}return sort;
		
	}
	
	public static void main (String[]args) {
	int[]ziffern= {9,4,7,9,3,1,6,8};
	System.out.println("Unsortiert");
	for(int i=0;i<ziffern.length;i++) {
		System.out.print(ziffern[i]+"/");
	}
	System.out.println();
	
	int[]nachalter=selectionsort(ziffern);
	System.out.println("Sortiert");
	for(int i=0;i<nachalter.length;i++) {
		System.out.print(nachalter[i]+"/");
	
  }
	
	}
}
