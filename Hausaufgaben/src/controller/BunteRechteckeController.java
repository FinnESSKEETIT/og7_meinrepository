//@Finn Schulte

package controller;

import java.util.LinkedList;
import java.util.List;

import model.MYSQLDatabase;
import model.Rechteck;
import view.GUI;

public class BunteRechteckeController {

	// Attribut
	private List rechtecke;
	private MYSQLDatabase database;

	// Konstruktor
	public BunteRechteckeController() {
		super();
		rechtecke = new LinkedList();
		this.database = new MYSQLDatabase();
	}

	public BunteRechteckeController(List rechtecke) {
		super();
		this.rechtecke = rechtecke;
	}

	// f�ge rechtecke hinzu
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechteck_eintragen(rechteck);
		
	}

	// l�scht alle Rechtecke
	public void reset() {
		rechtecke.clear();
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	// Getter und Setter
	public List getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void rechteckhinzufuegen() {
		// TODO Auto-generated method stub
		new GUI(this);
	}

}