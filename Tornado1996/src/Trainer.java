
public class Trainer extends Person{

private char lizenzklasse;
private int aufwandentschaedigung;



public Trainer(String name, char lizenzklasse, int aufwandentschaedigung) {
	super(name);
	this.lizenzklasse = lizenzklasse;
	this.aufwandentschaedigung = aufwandentschaedigung;
}

public char getLizenzklasse() {
	return lizenzklasse;
}
public void setLizenzklasse(char lizenzklasse) {
	this.lizenzklasse = lizenzklasse;
}
public int getAufwandentschaedigung() {
	return aufwandentschaedigung;
}
public void setAufwandentschaedigung(int aufwandentschaedigung) {
	this.aufwandentschaedigung = aufwandentschaedigung;
}


}
