
public class Spieler extends Person {

private String telefonnr;
private boolean jahresbeitragBezahl;
private int trikotnr;




public Spieler(String name, String telefonnr, boolean jahresbeitragBezahl, int trikotnr) {
	super(name);
	this.telefonnr = telefonnr;
	this.jahresbeitragBezahl = jahresbeitragBezahl;
	this.trikotnr = trikotnr;
}



public void setTelefonnr(String telefonnr) {
	this.telefonnr = telefonnr;
}
public String getTelefonnr() {
	return telefonnr;
}

public boolean isJahresbeitragBezahl() {
	return jahresbeitragBezahl;
}
public void setJahresbeitragBezahl(boolean jahresbeitragBezahl) {
	this.jahresbeitragBezahl = jahresbeitragBezahl;
}
public int getTrikotnr() {
	return trikotnr;
}
public void setTrikotnr(int trikotnr) {
	this.trikotnr = trikotnr;
}


}
