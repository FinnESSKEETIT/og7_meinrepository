/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 02.05.2018
  * @author 
  */

public class Person {
  
  // Anfang Attribute

  private String Anrede;
  private String Vorname;
  private String Nachname;
  private String Adresszusatz;
  private String Hausnummer;
  private String Land;
  private String Ort;
  private String Plz;
  private String Strasse;
  // Ende Attribute
  
  
  //Konstruktor
  public Person(String an, String vor, String nach, String adr, String hau, String lan, String or,String pl,String str ){
    Anrede=an;
    Vorname=vor;
    Nachname=nach;
    Adresszusatz=adr;
    Hausnummer=hau;
    Land=lan;
    Ort=or;
    Plz=pl;
    Strasse=str;
    }
 
  //Anfang Get u. Set  
  
  public String getAnrede() {
    return Anrede;
  }

  public String getVorname() {
    return Vorname;
  }

  public String getNachname() {
    return Nachname;
  }

  public String getAdresszusatz() {
    return Adresszusatz;
  }

  public String getHausnummer() {
    return Hausnummer;
  }

  public String getLand() {
    return Land;
  }

  public String getOrt() {
    return Ort;
  }

  public String getPlz() {
    return Plz;
  }

  public String getStrasse(){
     return Strasse;
    }
  // Ende Get u. Set
} // end of Person
