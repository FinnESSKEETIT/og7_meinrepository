package view;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import com.sun.glass.events.MouseEvent;

import controller.BunteRechteckeController;
import model.Rechteck;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private BunteRechteckeController ctr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI(BunteRechteckeController brc) {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		JTextPane txtpnX = new JTextPane();
		txtpnX.setText("X");
		contentPane.add(txtpnX);

		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);

		JTextPane txtpnY = new JTextPane();
		txtpnY.setText("Y");
		contentPane.add(txtpnY);

		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JTextPane txtpnBreite = new JTextPane();
		txtpnBreite.setText("Breite");
		contentPane.add(txtpnBreite);

		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JTextPane txtpnHhe = new JTextPane();
		txtpnHhe.setText("H\u00F6he");
		contentPane.add(txtpnHhe);

		textField_3 = new JTextField();
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JButton btnSpeichern = new JButton("Speichern");
		contentPane.add(btnSpeichern);
		
		this.ctr = brc;

		JButton btnHinzufgen = new JButton("Hinzuf\u00FCgen");
		btnHinzufgen.addMouseListener(new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
		int x = Integer.parseInt(textField_1.getText());
		int y = Integer.parseInt(textField_2.getText());
		int hoehe = Integer.parseInt(textField_3.getText());
		int breite = Integer.parseInt(textField_3.getText());
		Rechteck neu = new Rechteck(x, y, breite, hoehe);
		ctr.add(neu);
		}
		});
		contentPane.add(btnHinzufgen);
	}

}