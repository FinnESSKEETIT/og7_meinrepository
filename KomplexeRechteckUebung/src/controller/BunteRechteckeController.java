package controller;

import model.Rechteck;
import java.util.LinkedList;
import java.util.List;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	
	public BunteRechteckeController() {
		super();
		rechtecke=new LinkedList();
	}
	
	public List getRechtecke(){
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
}
