import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javafx.scene.control.Button;

import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TicTacToe_v2 extends JFrame {

private JPanel contentPane;

/**
* Launch the application.
*/
public static void main(String[] args) {
EventQueue.invokeLater(new Runnable() {
public void run() {
try {
TicTacToe_v2 frame = new TicTacToe_v2();
frame.setVisible(true);
} catch (Exception e) {
e.printStackTrace();
}
}
});
}

/**
* Create the frame.
*/
public TicTacToe_v2() {
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setBounds(100, 100, 450, 300);
contentPane = new JPanel();
contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
setContentPane(contentPane);
contentPane.setLayout(new GridLayout(3, 3, 0, 0));
for (int i = 0; i < 9; i++) {
JButton btn = new JButton("");
btn.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent arg0) {
changeState(btn);
}
});
contentPane.add(btn);
}
}
private void changeState (JButton btn) {
if (btn.getText().equals(""))
btn.setText("X");
else if (btn.getText().equals("X"))
btn.setText("O");
else 
btn.setText("");

}
}