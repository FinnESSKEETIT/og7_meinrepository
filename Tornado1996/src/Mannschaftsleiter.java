
public class Mannschaftsleiter {

private String mannschaftsname;
private int rabattJahresBeitrag;

public Mannschaftsleiter(String string, int rabattJahresBeitrag) {
	super();
	this.mannschaftsname = string;
	this.rabattJahresBeitrag = rabattJahresBeitrag;
}

public String getMannschaftsname() {
	return mannschaftsname;
}
public void setMannschaftsname(String mannschaftsname) {
	this.mannschaftsname = mannschaftsname;
}
public int getRabattJahresBeitrag() {
	return rabattJahresBeitrag;
}
public void setRabattJahresBeitrag(int rabattJahresBeitrag) {
	this.rabattJahresBeitrag = rabattJahresBeitrag;
}


}
